<?php
/**
 *  __    __
 * /\ \  /\ \
 * \ `\`\\/'/ ___     ___     ___      __
 *  `\ `\ /' / __`\  / __`\ /' _ `\  /'__`\
 *    `\ \ \/\ \L\ \/\ \L\ \/\ \/\ \/\  __/
 *      \ \_\ \____/\ \____/\ \_\ \_\ \____\
 *       \/_/\/___/  \/___/  \/_/\/_/\/____/
 *
 * Project: xmly, XML parser in PHP
 * File: datatypes.php
 *
 * Author: Yoann Bentz (Yoone)
 * Website: http://www.yoone.eu/
 * Contact: yoann@yoone.eu
 */


/*
 * The type of any xmly element, used for type casting
 */
class xmlyElement {}

/*
 * Root node, will only contain children
 */
class xmlyRoot extends xmlyElement
{
	protected $children = array();

	public $childCount = 0;

	protected function checkIndex($i, $count)
	{
		if (!is_int($i))
			throw new InvalidArgumentException('Integer expected');
		if ($i >= $count || $i < 0)
			throw new OutOfBoundsException("Parameter \$i ($i) is out of bounds");
	}

	public function child($i)
	{
		$this->checkIndex($i, $this->childCount);
		return $this->children[$i];
	}

	public function addChild(xmlyElement $newChild)
	{
		$this->children[$this->childCount++] = $newChild;
	}

	public function removeChild($i)
	{
		$this->checkIndex($i, $this->childCount);
		$this->children[$i] = null;
		unset($this->children[$i]);
		$this->children = array_values($this->children);
		$this->childCount--;
	}
}

/*
 * A classic XML node with a name, children and attributes
 */
class xmlyNode extends xmlyRoot
{
	private $attributes = array();

	public $attributeCount = 0;
	public $tagName = '';

	public function __construct($tagName)
	{
		$this->tagName = $tagName;
	}

	public function attribute($i)
	{
		$this->checkIndex($i, $this->attributeCount);
		return $this->attributes[$i];
	}

	public function addAttribute($name, $value)
	{
		$this->attributes[$this->attributeCount++] = new xmlyAttribute(strval($name), strval($value));
	}

	public function removeAttribute($i)
	{
		$this->checkIndex($i, $this->attributeCount);
		$this->attributes[$i] = null;
		unset($this->attributes[$i]);
		$this->attributes = array_values($this->attributes);
		$this->attributeCount--;
	}
}

/*
 * Simply text
 */
class xmlyText extends xmlyElement
{
	public $value = '';

	public function __construct($value)
	{
		$this->value = $value;
	}
}

/*
 * XML <!-- comment -->
 */
class xmlyComment extends xmlyText {}

/*
 * XML <![CDATA[]]>
 */
class xmlyCDATA extends xmlyText {}

/*
 * XML doctype <?xml?>
 */
class xmlyDoctype extends xmlyText {}

/*
 * HTML <!DOCTYPE>
 */
class xmlyDoctypeHTML extends xmlyText {}

/*
 * XML attribute="value", used in nodes
 */
class xmlyAttribute
{
	public $name = '';
	public $value = '';

	public function __construct($name, $value)
	{
		$this->name = $name;
		$this->value = $value;
	}
}
