<?php
/**
 *  __    __
 * /\ \  /\ \
 * \ `\`\\/'/ ___     ___     ___      __
 *  `\ `\ /' / __`\  / __`\ /' _ `\  /'__`\
 *    `\ \ \/\ \L\ \/\ \L\ \/\ \/\ \/\  __/
 *      \ \_\ \____/\ \____/\ \_\ \_\ \____\
 *       \/_/\/___/  \/___/  \/_/\/_/\/____/
 *
 * Project: xmly, XML parser in PHP
 * File: parser.php
 *
 * Author: Yoann Bentz (Yoone)
 * Website: http://www.yoone.eu/
 * Contact: yoann@yoone.eu
 */


/*
 * Checking data types
 */
if (!class_exists('xmlyElement'))
	require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'datatypes.php';


/*
 * XMLy Parser
 */
class xmly
{
	const InNone       = 0;
	const InTagName    = 1; // <
	const InSpecialTag = 2; // <!
	const InTag        = 3; // <[a-z][a-z0-9]*
	const InTagAttr    = 4; // <tag [a-z][a-z0-9\-]*
	const InTagAttrVal = 5; // <tag attr=
	const InClosingTag = 6; // </tag>

	private $xml; // XML code
	private $code_type; // xml|html
	private $root = null; // root node

	private $html_singletons = array( // HTML elements that don't always have a closing tag
		'area',
		'base',
		'br',
		'col',
		'command',
		'embed',
		'hr',
		'img',
		'input',
		'link',
		'meta',
		'param',
		'source',
		'wbr',
	);

	private $html_need_closing = array( // HTML elements that need a closing tag
		'a',
		'body',
		'colgroup',
		'dd',
		'div',
		'dt',
		'head',
		'html',
		'iframe',
		'li',
		'optgroup',
		'option',
		'p',
		'script',
		'span',
		'tbody',
		'td',
		'tfoot',
		'th',
		'thead',
		'title',
		'tr'
	);

	private $html_filter_tags = array( // Allowed HTML elements while filtering
		'!'          => array('comments' => false, 'doctype' => false),
		'a'          => array('href', 'target'),
		'blockquote' => null,
		'br'         => null,
		'code'       => null,
		'del'        => null,
		'div'        => array('align', 'style'),
		'em'         => null,
		'h1'         => null,
		'h2'         => null,
		'h3'         => null,
		'h4'         => null,
		'h5'         => null,
		'hr'         => null,
		'iframe'     => array('src', 'width', 'height', 'frameborder'),
		'img'        => array('src', 'width', 'height', 'alt', 'title', 'style'),
		'li'         => array(),
		'p'          => array('align', 'style'),
		'span'       => array('title', 'style'),
		'strong'     => array(),
		'table'      => array(), //TODO: table elts
		'u'          => array(),
		'ul'         => array()
	);

	/*
	 * Constructor, used only to initialize filter functions
	 */
	public function __construct($init = false)
	{
		if (!$init)
			return;

		$f_align = function($value) {
			$value = strtolower($value);
			return in_array($value, array('center', 'left', 'right', 'justify')) ? $value : null;
		};
		$f_size = function($value) {
			$value = intval($value);
			return $value > 0 ? $value : null;
		};
		$f_frameborder = function($value) {
			return 0;
		};
		$f_target = function($value) {
			return $value === '_blank' ? $value : null;
		};
		$f_style = function($value) {
			$css = null;
			$style = explode(';', $value);
			foreach ($style as $line)
			{
				$rule = array_map('trim', explode(':', $line));
				if (count($rule) === 2 && in_array($rule[0], array('margin-left', 'margin-right', 'width', 'height', 'float')))
					$css .= "$rule[0]:$rule[1];";
			}
			return $css;
		};

		foreach ($this->html_filter_tags as $tag => $attributes)
		{
			if (is_array($attributes) && $tag !== '!')
			{
				foreach ($attributes as $attr => $value)
				{
					$del_attr = true;

					switch ($value)
					{
						case 'style':
							$this->html_filter_tags[$tag][$value] = $f_style;
							break;
						case 'target':
							$this->html_filter_tags[$tag][$value] = $f_target;
							break;
						case 'align':
							$this->html_filter_tags[$tag][$value] = $f_align;
							break;
						case 'frameborder':
							$this->html_filter_tags[$tag][$value] = $f_frameborder;
							break;
						case 'width':
						case 'height':
							$this->html_filter_tags[$tag][$value] = $f_size;
							break;
						default:
							$del_attr = false;
							break;
					}

					if ($del_attr)
					{
						$this->html_filter_tags[$tag][$attr] = null;
						unset($this->html_filter_tags[$tag][$attr]);
					}
				}
			}
		}
	}

	/*
	 * Loading source file
	 */
	private static function loadFile($path)
	{
		if (!is_readable($path))
			throw new InvalidArgumentException("$path does not exist or is not readable");

		$handle = fopen($path, 'r');
		$contents = fread($handle, filesize($path));
		fclose($handle);

		return $contents;
	}

	/*
	 * Loading XML code
	 */
	public function loadXML($xml)
	{
		$this->xml = $xml;
		$this->code_type = 'xml';
	}

	/*
	 * Loading XML file
	 */
	public function loadXMLFile($path)
	{
		$this->loadXML(self::loadFile($path));
	}

	/*
	 * Loading HTML code
	 */
	public function loadHTML($html)
	{
		$this->xml = $html;
		$this->code_type = 'html';
	}

	/*
	 * Loading HTML file
	 */
	public function loadHTMLFile($path)
	{
		$this->loadHTML(self::loadFile($path));
	}

	/*
	 * Generates a useful temp array for the parsing
	 */
	private static function tmp()
	{
		return array(
			'open_tag' => '',
			'close_tag' => '',
			'current_attr' => '',
			'quote' => '',
			'attributes' => array(),
			'text' => ''
		);
	}

	/*
	 * Parsing the XML code
	 */
	public function parse($rtn = false)
	{
		if (empty($this->xml))
			throw new BadMethodCallException('No XML or HTML code has been loaded');

		$pos = self::InNone;
		$tmp = self::tmp();
		$c = '';
		$p = '';

		$this->root = new xmlyRoot();
		$stack = array($this->root);

		$len = strlen($this->xml);
		for ($i = 0; $i < $len; $i++)
		{
			$c = $this->xml[$i];
			switch ($pos)
			{
				case self::InTagName:
					if ($c === '!' || $c === '?')
						$pos = self::InSpecialTag;
					elseif (ctype_alpha($c) || $tmp['open_tag'] && ctype_digit($c))
						$tmp['open_tag'] .= $c;
					elseif (!trim($c))
						$pos = self::InTag;
					else
					{
						$i--;
						$pos = self::InTag;
					}
					break;
				case self::InSpecialTag:
					if ($tmp['open_tag'] === '--')
					{
						if ($p === '-' && $c === '-')
							$tmp['close_tag'] = '--';
						elseif ($p === '-' && $c === '>' && $tmp['close_tag'] === '--')
						{
							end($stack)->addChild(new xmlyComment(trim(rtrim($tmp['text'], '-'))));
							$tmp = self::tmp();
							$pos = self::InNone;
						}
						else
						{
							$tmp['close_tag'] = '';
							$tmp['text'] .= $c;
						}
					}
					elseif ($this->code_type === 'xml' && strtolower($tmp['open_tag']) === 'xml')
					{
						if ($p === '?' && $c === '>')
						{
							end($stack)->addChild(new xmlyDoctype(trim(rtrim($tmp['text'], '?'))));
							$tmp = self::tmp();
							$pos = self::InNone;
						}
						else
							$tmp['text'] .= $c;
					}
					elseif ($this->code_type === 'html' && strtolower($tmp['open_tag']) === 'doctype')
					{
						if ($c === '>')
						{
							end($stack)->addChild(new xmlyDoctypeHTML(trim($tmp['text'])));
							$tmp = self::tmp();
							$pos = self::InNone;
						}
						else
							$tmp['text'] .= $c;
					}
					elseif ($this->code_type === 'xml' && strtolower($tmp['open_tag']) === '[cdata[')
					{
						if ($p === ']' && $c === ']')
							$tmp['close_tag'] = ']]';
						elseif ($p === ']' && $c === '>' && $tmp['close_tag'] === ']]')
						{
							end($stack)->addChild(new xmlyCDATA(trim(rtrim($tmp['text'], ']'))));
							$tmp = self::tmp();
							$pos = self::InNone;
						}
						else
						{
							$tmp['close_tag'] = '';
							$tmp['text'] .= $c;
						}
					}
					elseif (ctype_alpha($c) || $c === '-' || $c === '[')
						$tmp['open_tag'] .= $c;
					else
						$pos = self::InNone;
					break;
				case self::InTag:
					if (ctype_alpha($c))
					{
						$tmp['current_attr'] = $c;
						$pos = self::InTagAttr;
					}
					elseif ($c === '>')
					{
						$node = new xmlyNode($tmp['open_tag']);
						foreach ($tmp['attributes'] as $name => $value)
						{
							$value = trim($value);
							if (!is_null($value))
								$node->addAttribute(trim($name), trim($value));
						}
						end($stack)->addChild($node);
						if ($p !== '/' && ($this->code_type !== 'html' || !in_array($tmp['open_tag'], $this->html_singletons)))
							array_push($stack, $node);
						$tmp = self::tmp();
						$pos = self::InNone;
					}
					elseif (!$tmp['open_tag'] && $c === '/')
						$pos = self::InClosingTag;
					break;
				case self::InClosingTag:
					if (ctype_alpha($c) || $tmp['close_tag'] && ctype_digit($c))
						$tmp['close_tag'] .= $c;
					else
					{
						if (isset(end($stack)->tagName) && $tmp['close_tag'] === end($stack)->tagName)
							array_pop($stack);
						$tmp['close_tag'] = '';
						$pos = self::InNone;
					}
					break;
				case self::InTagAttr:
					if (ctype_alpha($c) || ctype_digit($c) || $c === '-')
						$tmp['current_attr'] .= $c;
					elseif ($c === '=')
					{
						$tmp['attributes'][$tmp['current_attr']] = '';
						$pos = self::InTagAttrVal;
					}
					else
						$pos = self::InTag;
					break;
				case self::InTagAttrVal:
					if ($tmp['quote'])
					{
						if ($tmp['quote'] === true && (ctype_alpha($c) || ctype_digit($c)) || $tmp['quote'] !== true && ($c === $tmp['quote'] && $p === '\\' || $c !== $tmp['quote']))
							$tmp['attributes'][$tmp['current_attr']] .= $c;
						else
						{
							if ($tmp['quote'] === true)
								$i--;
							$tmp['quote'] = '';
							$tmp['current_attr'] = '';
							$pos = self::InTag;
						}
					}
					else
					{
						if ($c === '"' || $c === '\'')
							$tmp['quote'] = $c;
						else
						{
							$tmp['quote'] = true;
							$i--;
						}
					}
					break;
				default:
					if ($this->code_type === 'html' && isset(end($stack)->tagName) && end($stack)->tagName === 'script')
					{
						if ($tmp['quote'] && $c === $tmp['quote'])
							$tmp['quote'] = '';
						elseif ($c === '"' || $c === '\'')
							$tmp['quote'] = $c;
		
						if ($tmp['quote'] && $c === '<')
						{
							$tmp['text'] .= $c;
							break;
						}
					}

					if ($c === '<')
					{
						$tmp['text'] = trim($tmp['text']);
						if ($tmp['text'])
							end($stack)->addChild(new xmlyText($tmp['text']));
						$tmp['text'] = '';
						$pos = self::InTagName;
					}
					else
						$tmp['text'] .= $c;
					break;
			}
			$p = $c;
		}

		if ($rtn === true)
			return $this->root;
	}

	/*
	 * Check if a document has been parsed
	 */
	private function hasBeenParsed()
	{
		if (is_null($this->root))
			throw new BadMethodCallException('No document has been parsed');
	}

	/*
	 * Wrapper for the filtering method
	 */
	public function filter($tags = null)
	{
		$this->hasBeenParsed();
		if (empty($tags))
			$tags = $this->html_filter_tags;
		return $this->pfilter($this->root, $tags);
	}

	/*
	 * Printing with filtering XML/HTML tags
	 */
	private function pfilter(xmlyElement &$elt, array &$tags)
	{
		$opening_tag = '';
		$closing_tag = '';

		switch(get_class($elt))
		{
			case 'xmlyNode':
				if (array_key_exists($elt->tagName, $tags))
				{
					$opening_tag .= "<$elt->tagName";
					for ($i = 0; $i < $elt->attributeCount; $i++)
					{
						$attr = $elt->attribute($i);
						if (is_array($tags[$elt->tagName]))
						{
							if (array_key_exists($attr->name, $tags[$elt->tagName]))
							{
								$func = $tags[$elt->tagName][$attr->name];
								if (get_class($func) === 'Closure')
								{
									$val = $func($attr->value);
									if (!is_null($val))
										$opening_tag .= " $attr->name=\"$val\"";
								}
							}
							elseif (in_array($attr->name, $tags[$elt->tagName]))
								$opening_tag .= " $attr->name=\"$attr->value\"";
						}
					}

					if ($elt->childCount === 0 && ($this->code_type !== 'html' || !in_array($elt->tagName, $this->html_need_closing)))
						$opening_tag .= ' />';
					else
					{
						$opening_tag .= '>';
						$closing_tag = "</$elt->tagName>";
					}
				}
			case 'xmlyRoot':
				$children = '';
				for ($i = 0; $i < $elt->childCount; $i++)
				{
					$child = $this->pfilter($elt->child($i), $tags);

					$s_child = trim(preg_replace('#<[^>]+>#', '', $child));
					$s_children = trim(preg_replace('#<[^>]+>#', '', $children));
					if ($s_children && strpos('\'"’', substr($s_children, -1)) === false && $s_child && strpos('.,!?', $s_child[0]) === false)
						$children .= ' ';

					if ($child)
						$children .= $child;
				}
				return $opening_tag . $children . $closing_tag;
				break;
			case 'xmlyDoctype':
				if (isset($tags['!']['doctype']) && !$tags['!']['doctype'])
					break;
				return "<?xml $elt->value?>";
				break;
			case 'xmlyDoctypeHTML':
				if (isset($tags['!']['doctype']) && !$tags['!']['doctype'])
					break;
				return "<!DOCTYPE $elt->value>";
				break;
			case 'xmlyCDATA':
				return "<![CDATA[ $elt->value ]]>";
				break;
			case 'xmlyComment':
				if (isset($tags['!']['comments']) && !$tags['!']['comments'])
					break;
				return "<!-- $elt->value -->";
				break;
			case 'xmlyText':
				return $elt->value;
				break;
		}
	}

	/*
	 * Wrapper for the pretty printing method
	 */
	public function prettyPrint()
	{
		$this->hasBeenParsed();
		$this->pprint($this->root);
	}

	/*
	 * Pretty printing
	 */
	private function pprint(xmlyElement &$elt, $indent = -1, $display_nl = true)
	{
		$spaces = str_pad(null, $indent * 4);
		$type = get_class($elt);

		$opening_tag = '';
		$closing_tag = '';
		if ($type === 'xmlyNode')
		{
			$display_nl = $elt->childCount !== 1 || get_class($elt->child(0)) !== 'xmlyText';

			$opening_tag = "$spaces<$elt->tagName";
			for ($i = 0; $i < $elt->attributeCount; $i++)
			{
				$attr = $elt->attribute($i);
				$opening_tag .= " $attr->name=\"$attr->value\"";
			}

			if (!$display_nl)
			{
				$opening_tag .= ">";
				$closing_tag = "</$elt->tagName>\n";
			}
			elseif ($elt->childCount > 0)
			{
				$opening_tag .= ">\n";
				$closing_tag = "$spaces<$elt->tagName>\n";
			}
			elseif ($elt->childCount === 0 && $this->code_type === 'html' && in_array($elt->tagName, $this->html_need_closing))
				$opening_tag .= "><$elt->tagName>\n";
			else
				$opening_tag .= " />\n";
		}
		echo $opening_tag;

		if ($type === 'xmlyRoot' || $type === 'xmlyNode')
		{
			for ($i = 0; $i < $elt->childCount; $i++)
				$this->pprint($elt->child($i), $indent + 1, $display_nl);
		}
		elseif ($type === 'xmlyDoctype')
			echo "$spaces<?xml $elt->value?>\n";
		elseif ($type === 'xmlyDoctypeHTML')
			echo "$spaces<!DOCTYPE $elt->value>\n";
		elseif ($type === 'xmlyCDATA')
			echo "$spaces<![CDATA[ $elt->value ]]>\n";
		else
		{
			$opening = '';
			$closing = '';
			$lines = array_map('trim', explode("\n", $elt->value));

			if ($type === 'xmlyComment')
			{
				if (count($lines) > 1)
				{
					$opening = "$spaces<!-- $lines[0]\n";
					$lines[0] = '';
					$closing = $spaces . end($lines) . " -->\n";
					array_pop($lines);
				}
				else
				{
					$opening = "$spaces<!-- $lines[0] -->\n";
					$lines[0] = '';
				}
			}

			echo $opening;
			foreach ($lines as $line)
				if ($line !== '')
					echo $display_nl ? "$spaces$line\n" : "$line ";
			echo $closing;
		}

		echo $closing_tag;
	}
}
